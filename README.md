
### How to download from FTP kods ###
     setwd("C:\Users\dron\Desktop\examrae555")
    library(RCurl)
    library(XML)
    list <- getURL("ftp://ftp.ncdc.noaa.gov/pub/data/gsod/1969",dirlistonly=TRUE)

    FileList <- strsplit(list, split="\r\n")

    DIR <- paste(getwd(),"/MYEXAMDATAFILEs",sep="")
    dir.create(DIR)


    for FileName in unlist(FileList)){
        URL <- paste0("ftp://ftp.ncdc.noaa.gov/pub/data/gsod/1969",FileName)
        download.file(URL, destfile=paste0(DIR,"/",FileName), method="auto"),
            mode="wb")
    }    

    DownloadFile <- function(x){
        URL <- paste0("ftp://ftp.ncdc.noaa.gov/pub/data/gsod/1969",x)
        download.file(URL, destfile=paste0(DIR,"/",x), method="auto",
            mode="wb")
    }
    
    lapply(unlist(FileList)[1:5], DownloadFile)

    URL <- "ftp://ftp.ncdc.noaa.gov/pub/data/gsod/1969/gsod_1969.tar"
    download.file(URL,destfile=paste0(DIR,"/gsod_1969.tar"),
        method="auto",mode="wb")
    untar(paste0(getwd(),"/MYEXAMDATAFILEs/","gsod_21969.tar"),
        exdir=paste0(getwd(),"MYEXAMDATAFILEs"))
    help(unzip)

### Video kursa atskaite par to kā download no FTP server###

Pirmkart uztaisisim working directory:

    setwd("C:\Users\dron\Desktop\examrae555")

Pēc tam pievienosim vajadzīgas biblioteka:

    library(RCurl)
    library(XML)
    
Uztaisam sarakstu ar visiem filiem FTP serverī:

    list <- getURL("ftp://ftp.ncdc.noaa.gov/pub/data/gsod/1969",dirlistonly=TRUE)

Notirisim listu:

    FileList <- strsplit(list, split="\r\n")

Uztaisam jaunu mapi kur ielikt mūsu datus:

    DIR <- paste(getwd(),"/MYEXAMDATAFILEs",sep="")
    dir.create(DIR)

Nolupojam un dabujam visus datus:


    for FileName in unlist(FileList)){
        URL <- paste0("ftp://ftp.ncdc.noaa.gov/pub/data/gsod/1969",FileName)
        download.file(URL, destfile=paste0(DIR,"/",FileName), method="auto"),
            mode="wb")
    }    
Elegants veids:

    DownloadFile <- function(x){
        URL <- paste0("ftp://ftp.ncdc.noaa.gov/pub/data/gsod/1969",x)
        download.file(URL, destfile=paste0(DIR,"/",x), method="auto",
            mode="wb")
    }
    
    lapply(unlist(FileList)[1:5], DownloadFile)
    
Dabūt kopreseto failu:

    URL <- "ftp://ftp.ncdc.noaa.gov/pub/data/gsod/1969/gsod_1969.tar"
    download.file(URL,destfile=paste0(DIR,"/gsod_1969.tar"),
        method="auto",mode="wb")
    untar(paste0(getwd(),"/MYEXAMDATAFILEs/","gsod_21969.tar"),
        exdir=paste0(getwd(),"MYEXAMDATAFILEs"))
    help(unzip)
